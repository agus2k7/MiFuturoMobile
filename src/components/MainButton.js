import React from 'react';
import { Button, View, Text, TextInput, ListView, StyleSheet, ImageBackground, TouchableOpacity, Linking, ToastAndroid } from 'react-native';
// import { StackNavigator, Navigator } from 'react-navigation'; // Version can be specified in package.json
import { Establecimiento } from '../components/Establecimiento';
import { InfoButton } from '../components/InfoButton';
import FontAwesome, { Icons } from 'react-native-fontawesome';//font-awesome  
import { StackNavigator, withNavigation } from 'react-navigation'; // Version can be specified in package.json 

export default class MainButton extends React.Component {
    constructor(props) {
        super(props);
        const { onPress, disabled, title, color } = this.props
        // this.state = {
        //     disabled: (this.props.establecimiento.coordinates.longitude == 0 || this.props.establecimiento.coordinates.latitude == 0)
        // }
    }

    render(){        
        return (
            <TouchableOpacity
                onPress={this.props.onPress}
                style={[styles.button, (this.props.disabled) ? { backgroundColor: '#DFDFDF' } : { backgroundColor: this.props.color }]}
                disabled={this.props.disabled}
            >
                <Text style={[styles.text, (this.props.disabled) ? { color: '#AAAAAA' } : { color: 'white' }]}>
                    {(this.props.title).toUpperCase()}
                </Text>
            </TouchableOpacity>
        )
    }    
}

const styles = StyleSheet.create({
    text: {
        fontSize: 14,
        // color: "white",
        flex: 0,
        margin: 10,
    },
    button: {
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
        // width: 70,
        height: 40,
        borderColor: '#273819',
        borderRadius: 5,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 5,
        marginBottom: 10,
        borderBottomWidth: 3,
        borderRightWidth: 3,
    }
})