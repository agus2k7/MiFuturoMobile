import React from 'react';
import {Platform, Button, View, Text, TextInput, ActivityIndicator, ListView, StyleSheet, Image, ImageBackground, TouchableHighlight } from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';//font-awesome
import { StackNavigator, withNavigation } from 'react-navigation'; 

export class Establecimiento extends React.Component {
    constructor(props) {
        super(props); 
        const {row} = this.props  
           
    }

    onPress = () => {
        this.props.navigation.navigate('EstablecimientoDetalle', {
            detalle: this.props.row
        });
    }

    render() {
        return (
            <TouchableHighlight onPress={this.onPress} underlayColor={'#3A7F00'}>
                <View style={styles.container}>
                    
                        <View>                    
                            <Text style={styles.title}>{this.props.row.orientacion}</Text>
                        </View>
                        <View style={{flexDirection: 'row'}}>                                                
                                <Text style={styles.subtitle}>
                                    <FontAwesome>{Icons.institution}{" "}</FontAwesome>                                
                                    {this.props.row.nombre}
                                </Text>                                      
                        </View>
                    <View style={{ flexDirection: 'column', flexWrap: 'wrap', justifyContent: 'center', marginLeft: 10, marginRight: 10}}>
                            <Text style={styles.info}                           
                            >{"Localidad: "}
                                <Text style={styles.subtitle3}
                                numberOfLines={2}>
                                    {this.props.row.localidad}
                                </Text> 
                            </Text> 
                            <Text style={styles.info}                           
                            >{"Departamento: "}
                                <Text style={styles.subtitle3}>
                                    {this.props.row.departamento}
                                </Text> 
                            </Text> 
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', margin: 10}}>                    
                            <TouchableHighlight onPress={this.onPress} 
                                style={{alignContent: 'center', alignItems:'center'}}
                                underlayColor={'#3A7F00'}>
                                <Text style={styles.icon}>
                                    <FontAwesome>{Icons.plusCircle}</FontAwesome>   
                                    <Text style={styles.subtitle2}>{" Más Detalles"}</Text>                         
                                </Text>            
                            </TouchableHighlight>         
                        </View>
                    
                </View>
            </TouchableHighlight>
        );
    }
}    

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        height: 230,
        flexDirection: 'column',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        margin: 12,
        backgroundColor: '#ffffff',
        borderRadius: 12,
        borderTopColor: '#7fb850',
        borderTopWidth: 5,
        borderColor: '#7fb850',
        borderWidth: 1        
    },
    title: {
        margin: 10,
        fontSize: 20,
        textAlign: 'center',
        color: "#000000",
        ...Platform.select({
            android: {
                fontFamily: "SourceSansPro",
                },
            }), 
    },
    subtitle: {        
        fontSize: 15,
        textAlign: 'center',
        color: '#4c4c4c',
        ...Platform.select({
            android: {
                fontFamily: "SourceSansPro",
                },
            }),
        margin: 10 
    },
    subtitle2: {
        fontSize: 18,
        textAlign: 'center',
        color: '#4c4c4c',
        ...Platform.select({
            android: {
                fontFamily: "SourceSansPro",
                },
            }),
        margin: 10
    },
    info: {
        flexDirection: 'row',
        flexWrap: 'wrap',     
        marginLeft: 15,   
        fontSize: 14,
        textAlign: 'left',
        color: '#000000',
        fontFamily: "SourceSansPro-Bold", 
    },
    icon: {
        fontSize: 24,
        color: "#7fb850",
        textAlign: 'center',
        margin: 10,
    },
    subtitle3: {        
        ...Platform.select({
            android: {
                fontFamily: "SourceSansPro",
                },
            }),        
        flexWrap: 'wrap',
        color: '#4c4c4c'
    }
})