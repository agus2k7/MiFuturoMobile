import React from 'react';
import { Button, View, Text, TextInput, ListView, StyleSheet, ImageBackground, TouchableOpacity, Linking, ToastAndroid } from 'react-native';
// import { StackNavigator, Navigator } from 'react-navigation'; // Version can be specified in package.json
import { Establecimiento } from '../components/Establecimiento';
import { InfoButton } from '../components/InfoButton';
import FontAwesome, { Icons } from 'react-native-fontawesome';//font-awesome  
import { StackNavigator, withNavigation } from 'react-navigation'; // Version can be specified in package.json 

export class MapButton extends React.Component {
    constructor(props) {
        super(props);
        const { establecimiento } = this.props
        this.state = {
            disabled: (this.props.establecimiento.coordinates.longitude == 0 || this.props.establecimiento.coordinates.latitude == 0)
        }
    }
    
    render() {
        return (
            <TouchableOpacity
                onPress={this.onPress}
                style={[styles.button, (this.state.disabled) ? { backgroundColor: '#ABABAB' } : { backgroundColor: '#7fb850' }]}
                disabled={this.state.disabled}
            >
                <Text style={styles.icon}>                    
                        <FontAwesome>{Icons.mapMarker}</FontAwesome>                    
                </Text>
            </TouchableOpacity>
        );
    }

    onPress = (event) => {
        event.preventDefault()
        if (!(this.props.establecimiento.coordinates.longitude == 0 || this.props.establecimiento.coordinates.latitude == 0)) {            
            let aux = [this.props.establecimiento]            
            this.props.navigation.navigate('Mapa', {
                datos: aux
            })            
        }
    }
}

const styles = StyleSheet.create({
    icon: {
        fontSize: 35,
        color: "white",
        flex: 0,
        margin: 10,
    },
    button: {
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
        width: 70,
        height: 70,
        borderColor: '#273819',
        borderRadius: 70,
        margin: 20,
        borderBottomWidth: 3,
        borderRightWidth: 3,
    },
})