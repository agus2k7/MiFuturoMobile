import React from 'react';
import { Platform, ToastAndroid, Button, View, Text, TextInput, ActivityIndicator, ListView, StyleSheet, Image, ImageBackground, Dimensions, Switch, PermissionsAndroid, Promise } from 'react-native';
import { StackNavigator } from 'react-navigation'; // Version can be specified in package.json
import FontAwesome, { Icons } from 'react-native-fontawesome';//font-awesome
import InfoButton from '../components/InfoButton';
import API from '../api/API'
import Helper from '../utils/Helper'
import Permissions from 'react-native-permissions';
import Toast, {DURATION} from 'react-native-easy-toast';
import MainButton from '../components/MainButton'

export class PantallaInicio extends React.Component {
    constructor(props) {
        super(props);
        const { navigation } = this.props.navigation        
        this.state = {
            texto: '',
            respuestaJson: [],
            cargando: false,
            elementos: {},
            error: false,
            errorConnection: false,
            switchValue: false,
            latitud: -28.784948,
            longitud: -57.875989,
            permiso: false,
            errorGeo: null,
            loading: false
        }
    }

    static navigationOptions = ({ navigation }) => ({
        title: 'Mi Futuro',
        headerRight: (
            <InfoButton navigation={navigation}/>
        ),
        headerTintColor: '#fff',
        headerStyle: {
            backgroundColor: '#7fb850',
        },
        headerTitleStyle: {
            color: 'white'
        }
    })

    buscar = (palabras) => {
        this.setState({
            cargando: true,
            texto: '',
            errorConnection: false
        })
        let terminos = Helper.normalized(Helper.clearWords(palabras))               
        API.getOrientaciones(terminos)
        .then(responseJson => {
            let oriBuscadas = Helper.getIdsOrientaciones(responseJson, palabras);            
            buscarCues(oriBuscadas)
            })
        .catch((error) => {
            errorHandle(error, this)
        })

        //segunda peticion
        //busco los id de cue-anexos
        buscarCues = (oriBuscadas) => {
            API.getIdCues(oriBuscadas)
            .then(newResponseJson => {                
                var locSoloConOriBuscadas = Helper.filtrarEntrada(newResponseJson, terminos);
                let idCueAnexos = Helper.getIdsCueAnexos(newResponseJson)
                buscarInfoCues(locSoloConOriBuscadas,idCueAnexos)
            })
            .catch((error) => {
                errorHandle(error, this)
            })            
        }
        
        //tercera peticion
        buscarInfoCues = (locSoloConOriBuscadas,idCueAnexos) => {
            API.getEstablecimientos(idCueAnexos)
            .then(cueAnexosResponseJson => {
                let allSchools = Helper.mergearEscuelas(locSoloConOriBuscadas, cueAnexosResponseJson);
                //busco establecimientos cercanos
                if(this.state.switchValue){
                    if(this.state.longitud != 0){                        
                        filtrarCercanos(allSchools, this.state.longitud,this.state.latitud, 120)
                    }
                }else{
                    prepararSalida(allSchools)
                }
            })
            .catch((error) => {
                errorHandle(error, this)
            }) 

        }

        filtrarCercanos = (allSchools, long, lat, distancia) => {
            API.getEstablecimientosCercanos(lat, long, distancia)
            .then(cercanos =>{
                let cercanosId = [];
                cercanos.content.forEach(function (v, index, initial_array) {
                    cercanosId.push(v.content.id)
                })                
                var filtrado = allSchools.filter(function(item) {
                    return cercanosId.indexOf(item.id) !== -1;
                });
                prepararSalida(filtrado)
            })
            .catch((error) => {
                errorHandle(error, this)
            }) 
        }
        //cargar listado escuela (armo un nuevo json con los datos a mostrar)
        prepararSalida = (allSchools) => {
            let aux = []
            aux = Helper.armarJson(allSchools)            
            this.setState({                
                respuestaJson: aux,
                cargando: false
            })
            this.setState({
                switchValue: false
            })
            this.props.navigation.navigate('Detalle', {
                busqueda: this.state.respuestaJson
            });
        }
        //si hubo errores lo envío de nuevo al inicio
        if(this.state.errorConnection){
            this.props.navigation.navigate('Inicio');
        }
    }       
    
    componentDidMount() {
        if (Platform.Version < 23) {
            this.setState({
                permiso: true
            })
        }
        setTimeout(() => {
            Platform.OS === 'ios' ? requestGeoPermissionIOS(this) : requestGeoPermissionAndroid(this)            
        }, 1000);        
    }

    onChanged(text){
        let nuevoTexto = Helper.checkText(text, this)
        this.setState({ texto: nuevoTexto })    
    }   

    render() {        
        if (this.state.cargando) {
            return (
                <View style={styles.Container}>
                    <ActivityIndicator
                        animating={true}
                        color='#7fb850'
                        size='large'
                    />
                </View>
            );
        }
        return (
            <ImageBackground source={require('../img/bgtransp.jpg')}
                style={styles.PantallaInicio}
                imageStyle={styles.backgroundImage}>


                <View style={styles.Container}>
                    <Text style={styles.mainTitle}>
                        <Text>
                            MI</Text>
                        <Text style={styles.bold}>
                            FUTURO</Text>
                    </Text>
                    <View
                        style={styles.line}
                    />
                    <Text style={styles.title}>
                        EL INICIO DE TU CAMINO SE ENCUENTRA AQUÍ. DESCUBRILO.</Text>
                    <TextInput
                        style={styles.TextInputStyle}
                        placeholder="Ingresa tu orientación..."
                        underlineColorAndroid='transparent'
                        placeholderTextColor='white'
                        keyboardType={'email-address'}
                        autoCorrect={false}
                        clearButtonMode={'while-editing'}  
                        editable={!this.state.loading}                      
                        onChangeText={(cadena) => {
                                        this.setState({ texto: cadena })
                                        this.onChanged(cadena)
                                    }}
                        value = {this.state.texto}
                    />
                    {!!this.state.error && (
                        <Text style={{ color: 'red' }}>{this.state.error}</Text>
                    )}
                    <MainButton
                        title="Buscar Carreras"
                        color="#7fb850"
                        disabled={this.state.loading}
                        onPress={() => {
                            if (this.state.texto.trim() === "") {
                                this.setState(() => ({ error: "Se requiere una palabra." }));
                                this.setState(() => ({ texto: "" }));
                            } else if (this.state.texto.trim().length < 5) {
                                this.setState(() => ({ error: "Se requiere una palabra con más de 4 caracteres." }));
                                this.setState(() => ({ texto: "" }));
                            } else {
                                this.setState(() => ({ error: null }))
                                this.buscar(this.state.texto)
                            }
                        }}
                    />
                    <View style={{flexDirection: 'row', margin: 10}}>
                        <Switch
                            onValueChange={(value) => 
                                {this.setState({ switchValue: value })
                                    if(value){                                        
                                        getPosition(this)
                                    }
                                }
                            }
                            value={this.state.switchValue}
                            disabled={this.state.loading}
                            tintColor= '#0009'
                            onTintColor= '#7fb850'
                            thumbTintColor= '#ffffff'
                        />
                        <Text style={styles.text}>{' Buscar establecimientos cercanos'}</Text>
                        {this.state.loading && (
                        <ActivityIndicator
                            animating={true}
                            color='#ffffff'
                            size='small'
                        />)}
                    </View>
                    <Toast
                        ref='toast'
                        position='bottom'
                        positionValue={200}
                        // fadeInDuration={750}
                        // fadeOutDuration={1000}
                        opacity={0.8}
                    />
                </View>
            </ImageBackground>

        );
    }

}

const styles = StyleSheet.create({
    PantallaInicio: {
        flex: 1,
        //margin: 10,
        flexDirection: 'column',
        justifyContent: 'center',
        //alignItems: 'flex-end',
    },
    Container: {
        flex: 2,
        margin: 10,
        flexDirection: 'column',
        justifyContent: 'center',
        //alignItems: 'flex-end',
    },
    bold: {
        fontFamily: "Montserrat-Bold",
        color: "#ffffff",
    },
    mainTitle: {
        fontSize: 45,
        padding: 10,
        textAlign: 'center',
        color: "#ffffffb3",
        fontFamily: "Montserrat",
    },
    title: {
        fontSize: 18,
        //padding: 10,
        textAlign: 'center',
        color: "#ffffff",
        fontFamily: "SourceSansPro-Light",
    },
    TextInputStyle: {
        margin: 10,
        textAlign: 'center',
        color: 'white',
        height: 40,
        borderWidth: 0.5,
        borderColor: 'white',
        borderRadius: 7,
        backgroundColor: '#0009'      
    },
    imageHeader: {
        // flexDirection: 'row',
        // justifyContent: 'flex-start',
        // margin: 20,
        // resizeMode: 'cover'
    },
    backgroundImage: {
        flex: 1,
        width: null,
        height: null,
        //resizeMode: 'cover'
    },
    line: {
        margin: 30,
        borderBottomColor: 'white',
        borderBottomWidth: 0.5,
    },
    text: {
        fontSize: 14,
        textAlign: 'left',
        color: '#ffffff',
        ...Platform.select({
            android: {
                fontFamily: "SourceSansPro",
                },
            }),
        margin: 10
    },
});

const getPosition = async (contex) => {
    contex.setState({
        loading: true
    })
    // console.log('******* permiso:' + contex.state.permiso + '*******')
    if(contex.state.permiso){  
        if (Platform.Version < 23) {
            await navigator.geolocation.getCurrentPosition(
                (position) => {
                    contex.setState({
                        latitud: position.coords.latitude,
                        longitud: position.coords.longitude,
                        errorGeo: null,
                    });
                },
                (error) => contex.setState({ errorGeo: error.message }),
                { enableHighAccuracy: false, timeout: 15000 },
            );
        } else {
            await navigator.geolocation.getCurrentPosition(
                    (position) => {
                        contex.setState({
                        latitud: position.coords.latitude,
                        longitud: position.coords.longitude,
                        errorGeo: null,
                      });
                    },
                    (error) => contex.setState({ errorGeo: error.message }),
                    { enableHighAccuracy: true, timeout: 15000 },
                  );            
        }      
        // contex.watchID = await navigator.geolocation.watchPosition(
        //     (position) => {
        //         contex.setState({
        //         latitud: position.coords.latitude,
        //         longitud: position.coords.longitude,
        //         errorGeo: null,
        //       });
        //     },
        //     (error) => contex.setState({ errorGeo: error.message }),
        //     { enableHighAccuracy: true, timeout: 20000 },
        //   );
    }else{
        // console.log('******* No se concedieron los permisos de Geolocalización *******')
        // contex.toastMessage('No se concedieron los permisos de Geolocalización', ToastAndroid.LONG, ToastAndroid.BOTTOM) 
        contex.refs.toast.show('No se concedieron los permisos de Geolocalización', 3000);
    }
    // console.log('lat ' + contex.state.latitud + ' long ' + contex.state.longitud)
    // console.log('loading 1 ' + contex.state.loading)
    setTimeout(() => {
        if(contex.state.longitud == -57.875989 || contex.state.latitud == -28.784948){
            // console.log('error al recuperar la geo')
            contex.setState({
                switchValue: false
            })
            // contex.toastMessage('No se pudo obtener su ubicación, verifique si se encuentra encendido su GPS e inténtelo nuevamente', ToastAndroid.LONG, ToastAndroid.BOTTOM)
            // console.log('******* No se pudo obtener su ubicación, verifique si se encuentra encendido su GPS e inténtelo nuevamente *******')
            contex.refs.toast.show('No se pudo obtener su ubicación, verifique si se encuentra encendido su GPS e inténtelo nuevamente',4000);
        }
        // console.log('latitud ' + contex.state.latitud)
        // console.log('longitud '+ contex.state.longitud)
        contex.setState({
            loading: false
        })
        // console.log('loading 2 ' + contex.state.loading)
    },10000)
}

const requestGeoPermissionAndroid = async (ctx) => {    
    const chckLocationPermission = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)    
    if (chckLocationPermission) {
        // alert('se concedieron los permisos') 
        ctx.setState({
            permiso: true
        })       
    } else {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    'title': 'Permiso de geolocalización',
                    'message': 'Se requiere permiso para acceder a la geolocalización de tu dispositivo para usar esta característica'
                }
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                // alert("You can use geo")
                ctx.setState({
                    permiso: true
                })
            } 
            // else {
            //     ctx.setState({
            //         permiso: false
            //     })
            // }
        } catch (err) {
            // console.warn(err)
        }
    }   
}

const requestGeoPermissionIOS = (ctx) => {
    Permissions.request('location', { type: 'always' }).then(response => {
        switch (response) {
            case 'authorized':
                {
                    ctx.setState({ permiso: true })
                    // console.log('concediendo permiso')
                }

        }
        // console.log('****IOS PERMISSIONS***' + response)
        // console.log('******* estado del permiso:' + ctx.state.permiso + '*******')
    })
}    

setPosition = (position) => {   
    this.setState({
        latitud: position.coords.latitude,
        longitud: position.coords.longitude
    })
}

errorHandle = (error, ctx) => {
    // console.log('error', error)
    // alert(error)
    ctx.setState({
        cargando: false,
        errorConnection: true
    })
    ctx.refs.toast.show('Se perdió la conexión, inténtelo nuevamente', 3000);
    throw new Error(error)
}