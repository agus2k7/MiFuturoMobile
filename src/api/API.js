// aca va todo lo de la API y algunas cosas mas
import React from 'react';
import Helper from '../utils/Helper'

var locOrientaciones = "http://200.32.52.6:8083/api/localizacionesOrientaciones/?text="
var localizaciones = "http://200.32.52.6:8082/api/localizacion/";
var orientaciones = 'http://200.32.52.6:8083/api/localizacionesOrientaciones?orientaciones='
var orientacion = 'http://200.32.52.6:8083/api/orientacion'

export default class API extends React.Component{
    static getOrientaciones = (terminos) => {
        let uri = orientacion + '?regex=' + terminos
        return fetch(uri).then(function (response) {
            if (response.ok) {
                return response.json()
            } else {
                throw new Error(response.status)
            }
        })
    }
    static getIdCues = (oriBuscadas) => {
        let uri = orientaciones + oriBuscadas.toString()
        return fetch(uri).then(function (response) {
            if (response.ok) {
                return response.json()
            } else {
                throw new Error(response.status)
            }
        })
    }
    static getEstablecimientos = (idCueAnexos) => {
        let uri = localizaciones + '?ids=' + idCueAnexos
        return fetch(uri).then(function (response) {
            if (response.ok) {
                return response.json()
            } else {
                throw new Error(response.status)
            }
        })
    }
    static getEstablecimientosCercanos = (lat, long, distancia) => {
        let uri = localizaciones + 'near/' + long + ',' + lat + '/' + distancia
        return fetch(uri).then(function (response) {
            if (response.ok) {
                return response.json()
            } else {
                throw new Error(response.status)
            }
        })
    }
}