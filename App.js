import React from 'react';
import { Button, View, Text } from 'react-native';
import { createStackNavigator } from 'react-navigation'; // Version can be specified in package.json
import { PantallaDetalle } from './src/screens/PantallaDetalle';
import { PantallaInicio } from './src/screens/PantallaInicio';
import { About } from './src/screens/About';
import { EstablecimientoDetalle } from './src/screens/PantallaEstablecimientoDetalle'
import { withNavigationPreventDuplicate } from './src/utils/navigation'
import { PantallaMapa } from './src/screens/PantallaMapa';
import SplashScreen from 'react-native-splash-screen'


const RootStack = createStackNavigator(
  {
    Inicio: {
      screen: PantallaInicio,
    },
    Detalle: {
      screen: PantallaDetalle,
    },
    About: {
      screen: About,
    },
    EstablecimientoDetalle: {
      screen: EstablecimientoDetalle,
    },
    Mapa: {
      screen: PantallaMapa,
    },
  },
  {
    initialRouteName: 'Inicio',
  }
);

RootStack.router.getStateForAction = withNavigationPreventDuplicate(
  RootStack.router.getStateForAction
);


export default class App extends React.Component {

  componentDidMount() {
      SplashScreen.hide();
  }

  render() {
    return <RootStack />;
  }  
}
